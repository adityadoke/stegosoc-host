

default['stegosoc']['wazuh-manager']['hostname'] = 'hound-testing-1799615984.us-east-1.elb.amazonaws.com'


default['stegosoc']['wazuh-manager']['port'] = '1515'

default['stegosoc']['coon']['agent_unique_name'] = "#{node['hostname']}".tr("-",'')
default['stegosoc']['coon']['syscheck_frequency'] = '3600'

node.default['apt']['compile_time_update'] = true


